-- phpMyAdmin SQL Dump
-- version 4.6.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июн 01 2016 г., 17:38
-- Версия сервера: 10.0.23-MariaDB
-- Версия PHP: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `AUTO_FACTORY`
--

-- --------------------------------------------------------

--
-- Структура таблицы `T_AUTO`
--

CREATE TABLE `T_AUTO` (
  `ID` int(11) NOT NULL,
  `ID_BODY` int(11) NOT NULL,
  `ID_ENGINE` int(11) NOT NULL,
  `ID_TRANSMISSION` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `T_BODY`
--

CREATE TABLE `T_BODY` (
  `ID` int(11) NOT NULL,
  `ID_BODY_TYPE` int(11) NOT NULL,
  `ID_COLOR` int(11) NOT NULL,
  `DOOR_COUNT` int(11) DEFAULT NULL,
  `VIN` varchar(17) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `T_BODY_TYPE`
--

CREATE TABLE `T_BODY_TYPE` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `T_COLOR`
--

CREATE TABLE `T_COLOR` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `T_ENGINE`
--

CREATE TABLE `T_ENGINE` (
  `ID` int(11) NOT NULL,
  `ID_ENGINE_TYPE` int(11) NOT NULL,
  `VOLUME` int(11) DEFAULT NULL,
  `POWER` int(11) DEFAULT NULL,
  `SN` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `T_ENGINE_TYPE`
--

CREATE TABLE `T_ENGINE_TYPE` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `T_TRANSMISSION`
--

CREATE TABLE `T_TRANSMISSION` (
  `ID` int(11) NOT NULL,
  `ID_TRANSMISSION_TYPE` int(11) NOT NULL,
  `SN` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `T_TRANSMISSION_TYPE`
--

CREATE TABLE `T_TRANSMISSION_TYPE` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `T_AUTO`
--
ALTER TABLE `T_AUTO`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_T_AUTO_T_BODY` (`ID_BODY`),
  ADD KEY `FK_T_AUTO_T_ENGINE` (`ID_ENGINE`),
  ADD KEY `FK_T_AUTO_T_TRANSMISSION` (`ID_TRANSMISSION`);

--
-- Индексы таблицы `T_BODY`
--
ALTER TABLE `T_BODY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_T_BODY_T_BODY_TYPE` (`ID_BODY_TYPE`),
  ADD KEY `FK_T_BODY_T_COLOR` (`ID_COLOR`);

--
-- Индексы таблицы `T_BODY_TYPE`
--
ALTER TABLE `T_BODY_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `T_COLOR`
--
ALTER TABLE `T_COLOR`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `T_ENGINE`
--
ALTER TABLE `T_ENGINE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_T_ENGINE_T_ENGINE_TYPE` (`ID_ENGINE_TYPE`);

--
-- Индексы таблицы `T_ENGINE_TYPE`
--
ALTER TABLE `T_ENGINE_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `T_TRANSMISSION`
--
ALTER TABLE `T_TRANSMISSION`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_T_TRANSMIS_T_TRANSMIS_TYPE` (`ID_TRANSMISSION_TYPE`);

--
-- Индексы таблицы `T_TRANSMISSION_TYPE`
--
ALTER TABLE `T_TRANSMISSION_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `T_AUTO`
--
ALTER TABLE `T_AUTO`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `T_BODY`
--
ALTER TABLE `T_BODY`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `T_BODY_TYPE`
--
ALTER TABLE `T_BODY_TYPE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `T_COLOR`
--
ALTER TABLE `T_COLOR`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `T_ENGINE`
--
ALTER TABLE `T_ENGINE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `T_ENGINE_TYPE`
--
ALTER TABLE `T_ENGINE_TYPE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `T_TRANSMISSION`
--
ALTER TABLE `T_TRANSMISSION`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `T_TRANSMISSION_TYPE`
--
ALTER TABLE `T_TRANSMISSION_TYPE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
