package com.mycompany.entity;

import com.mycompany.*;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author defender
 */
@Entity
@Table(name = "T_BODY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TBody.findAll", query = "SELECT t FROM TBody t"),
    @NamedQuery(name = "TBody.findById", query = "SELECT t FROM TBody t WHERE t.id = :id"),
    @NamedQuery(name = "TBody.findByIdBodyType", query = "SELECT t FROM TBody t WHERE t.idBodyType = :idBodyType"),
    @NamedQuery(name = "TBody.findByIdColor", query = "SELECT t FROM TBody t WHERE t.idColor = :idColor"),
    @NamedQuery(name = "TBody.findByDoorCount", query = "SELECT t FROM TBody t WHERE t.doorCount = :doorCount"),
    @NamedQuery(name = "TBody.findByVin", query = "SELECT t FROM TBody t WHERE t.vin = :vin")})
public class TBody implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_BODY_TYPE")
    private int idBodyType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_COLOR")
    private int idColor;
    @Column(name = "DOOR_COUNT")
    private Integer doorCount;
    @Size(max = 17)
    @Column(name = "VIN")
    private String vin;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(
            name="ID_BODY_TYPE", 
            referencedColumnName = "ID", 
            insertable = false, 
            updatable = false)
    protected TBodyType bodyType;    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(
            name="ID_COLOR", 
            referencedColumnName = "ID", 
            insertable = false, 
            updatable = false)
    protected TColor color;
    

    public TBody() {
    }

    public TBody(Integer id) {
        this.id = id;
    }

    public TBody(Integer id, int idBodyType, int idColor) {
        this.id = id;
        this.idBodyType = idBodyType;
        this.idColor = idColor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdBodyType() {
        return idBodyType;
    }

    public void setIdBodyType(int idBodyType) {
        this.idBodyType = idBodyType;
    }

    public int getIdColor() {
        return idColor;
    }

    public void setIdColor(int idColor) {
        this.idColor = idColor;
    }

    public Integer getDoorCount() {
        return doorCount;
    }

    public void setDoorCount(Integer doorCount) {
        this.doorCount = doorCount;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public TBodyType getBodyType() {
        return bodyType;
    }

    public void setBodyType(TBodyType bodyType) {
        this.bodyType = bodyType;
    }

    public TColor getColor() {
        return color;
    }

    public void setColor(TColor color) {
        this.color = color;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TBody)) {
            return false;
        }
        TBody other = (TBody) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.TBody[ id=" + id + " ]";
    }
    
}
