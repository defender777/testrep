package com.mycompany.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author defender
 */
@Entity
@Table(name = "T_TRANSMISSION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TTransmission.findAll", query = "SELECT t FROM TTransmission t"),
    @NamedQuery(name = "TTransmission.findById", query = "SELECT t FROM TTransmission t WHERE t.id = :id"),
    @NamedQuery(name = "TTransmission.findByIdTransmissionType", query = "SELECT t FROM TTransmission t WHERE t.idTransmissionType = :idTransmissionType"),
    @NamedQuery(name = "TTransmission.findBySn", query = "SELECT t FROM TTransmission t WHERE t.sn = :sn")})
public class TTransmission implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_TRANSMISSION_TYPE")
    private int idTransmissionType;
    @Size(max = 255)
    @Column(name = "SN")
    private String sn;    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(
            name="ID_TRANSMISSION_TYPE", 
            referencedColumnName = "ID", 
            insertable = false, 
            updatable = false)
    protected TTransmissionType transmissionType;
    
    public TTransmission() {
    }

    public TTransmission(Integer id) {
        this.id = id;
    }

    public TTransmission(Integer id, int idTransmissionType) {
        this.id = id;
        this.idTransmissionType = idTransmissionType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdTransmissionType() {
        return idTransmissionType;
    }

    public void setIdTransmissionType(int idTransmissionType) {
        this.idTransmissionType = idTransmissionType;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public TTransmissionType getTransmissionType() {
        return transmissionType;
    }

    public void setTransmissionType(TTransmissionType transmissionType) {
        this.transmissionType = transmissionType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TTransmission)) {
            return false;
        }
        TTransmission other = (TTransmission) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.TTransmission[ id=" + id + " ]";
    }
    
}
