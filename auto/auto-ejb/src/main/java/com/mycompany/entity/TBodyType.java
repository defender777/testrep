package com.mycompany.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author defender
 */
@Entity
@Table(name = "T_BODY_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TBodyType.findAll", query = "SELECT t FROM TBodyType t"),
    @NamedQuery(name = "TBodyType.findById", query = "SELECT t FROM TBodyType t WHERE t.id = :id"),
    @NamedQuery(name = "TBodyType.findByName", query = "SELECT t FROM TBodyType t WHERE t.name = :name")})
public class TBodyType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;

    public TBodyType() {
    }

    public TBodyType(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TBodyType)) {
            return false;
        }
        TBodyType other = (TBodyType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.TBodyType[ id=" + id + " ]";
    }
    
}
