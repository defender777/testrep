package com.mycompany.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author defender
 */
@Entity
@Table(name = "T_AUTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TAuto.findAll", query = "SELECT t FROM TAuto t"),
    @NamedQuery(name = "TAuto.findById", query = "SELECT t FROM TAuto t WHERE t.id = :id"),
    @NamedQuery(name = "TAuto.findByIdBody", query = "SELECT t FROM TAuto t WHERE t.idBody = :idBody"),
    @NamedQuery(name = "TAuto.findByIdEngine", query = "SELECT t FROM TAuto t WHERE t.idEngine = :idEngine"),
    @NamedQuery(name = "TAuto.findByIdTransmission", query = "SELECT t FROM TAuto t WHERE t.idTransmission = :idTransmission")})
public class TAuto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_BODY")
    private int idBody;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ENGINE")
    private int idEngine;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_TRANSMISSION")
    private int idTransmission;
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(
            name="ID_BODY", 
            referencedColumnName = "ID", 
            insertable = false, 
            updatable = false)
    protected TBody body;    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(
            name="ID_ENGINE", 
            referencedColumnName = "ID", 
            insertable = false, 
            updatable = false)
    protected TEngine engine;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(
            name="ID_TRANSMISSION", 
            referencedColumnName = "ID", 
            insertable = false, 
            updatable = false)
    protected TTransmission transmission;
    
    public TAuto() {
    }

    public TAuto(Integer id) {
        this.id = id;
    }

    public TAuto(Integer id, int idBody, int idEngine, int idTransmission) {
        this.id = id;
        this.idBody = idBody;
        this.idEngine = idEngine;
        this.idTransmission = idTransmission;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdBody() {
        return idBody;
    }

    public void setIdBody(int idBody) {
        this.idBody = idBody;
    }

    public int getIdEngine() {
        return idEngine;
    }

    public void setIdEngine(int idEngine) {
        this.idEngine = idEngine;
    }

    public int getIdTransmission() {
        return idTransmission;
    }

    public void setIdTransmission(int idTransmission) {
        this.idTransmission = idTransmission;
    }

    public TBody getBody() {
        return body;
    }

    public void setBody(TBody body) {
        this.body = body;
    }

    public TEngine getEngine() {
        return engine;
    }

    public void setEngine(TEngine engine) {
        this.engine = engine;
    }

    public TTransmission getTransmission() {
        return transmission;
    }

    public void setTransmission(TTransmission transmission) {
        this.transmission = transmission;
    }  
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TAuto)) {
            return false;
        }
        TAuto other = (TAuto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.TAuto[ id=" + id + " ]";
    }
    
}
