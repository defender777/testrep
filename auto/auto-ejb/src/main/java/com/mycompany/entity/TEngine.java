package com.mycompany.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author defender
 */
@Entity
@Table(name = "T_ENGINE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TEngine.findAll", query = "SELECT t FROM TEngine t"),
    @NamedQuery(name = "TEngine.findById", query = "SELECT t FROM TEngine t WHERE t.id = :id"),
    @NamedQuery(name = "TEngine.findByIdEngineType", query = "SELECT t FROM TEngine t WHERE t.idEngineType = :idEngineType"),
    @NamedQuery(name = "TEngine.findByVolume", query = "SELECT t FROM TEngine t WHERE t.volume = :volume"),
    @NamedQuery(name = "TEngine.findByPower", query = "SELECT t FROM TEngine t WHERE t.power = :power"),
    @NamedQuery(name = "TEngine.findBySn", query = "SELECT t FROM TEngine t WHERE t.sn = :sn")})
public class TEngine implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ENGINE_TYPE")
    private int idEngineType;
    @Column(name = "VOLUME")
    private Integer volume;
    @Column(name = "POWER")
    private Integer power;
    @Size(max = 255)
    @Column(name = "SN")
    private String sn;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(
            name="ID_ENGINE_TYPE", 
            referencedColumnName = "ID", 
            insertable = false, 
            updatable = false)
    protected TEngineType engineType;
    
    public TEngine() {
    }

    public TEngine(Integer id) {
        this.id = id;
    }

    public TEngine(Integer id, int idEngineType) {
        this.id = id;
        this.idEngineType = idEngineType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdEngineType() {
        return idEngineType;
    }

    public void setIdEngineType(int idEngineType) {
        this.idEngineType = idEngineType;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public String getSn() {
        return sn;
    }

    public TEngineType getEngineType() {
        return engineType;
    }

    public void setEngineType(TEngineType engineType) {
        this.engineType = engineType;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TEngine)) {
            return false;
        }
        TEngine other = (TEngine) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.TEngine[ id=" + id + " ]";
    }
    
}
