package com.mycompany.facade;

import com.mycompany.faces.ITransmissionType;
import com.mycompany.entity.TTransmissionType;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author defender
 */
@Stateless
public class TTransmissionTypeFacade extends AFacade<TTransmissionType> implements ITransmissionType {

    @PersistenceContext(unitName = "auto-unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TTransmissionTypeFacade() {
        super(TTransmissionType.class);
    }
    
}
