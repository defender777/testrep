package com.mycompany.facade;

import com.mycompany.faces.IColor;
import com.mycompany.entity.TColor;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author defender
 */
@Stateless
public class TColorFacade extends AFacade<TColor> implements IColor {

    @PersistenceContext(unitName = "auto-unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TColorFacade() {
        super(TColor.class);
    }
    
}
