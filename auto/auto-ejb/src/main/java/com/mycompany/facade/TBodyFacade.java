package com.mycompany.facade;

import com.mycompany.entity.TAuto;
import com.mycompany.faces.IBody;
import com.mycompany.entity.TBody;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author defender
 */
@Stateless
public class TBodyFacade extends AFacade<TBody> implements IBody {

    @PersistenceContext(unitName = "auto-unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TBodyFacade() {
        super(TBody.class);
    }

    @Override
    public boolean isAvailable(int id) {                
        Integer count = (Integer) em.createNativeQuery("select count(*) from T_AUTO where ID=?", Integer.class).setParameter(1, id).getSingleResult();
        return (count == 0);        
    }
    
}
