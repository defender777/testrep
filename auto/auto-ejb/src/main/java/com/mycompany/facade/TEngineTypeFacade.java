package com.mycompany.facade;

import com.mycompany.faces.IEngineType;
import com.mycompany.entity.TEngineType;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author defender
 */
@Stateless
public class TEngineTypeFacade extends AFacade<TEngineType> implements IEngineType {

    @PersistenceContext(unitName = "auto-unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TEngineTypeFacade() {
        super(TEngineType.class);
    }
    
}
