package com.mycompany.facade;

import com.mycompany.faces.IAuto;
import com.mycompany.entity.TAuto;
import java.math.BigInteger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author defender
 */
@Stateless
public class TAutoFacade extends AFacade<TAuto> implements IAuto {

    @PersistenceContext(unitName = "auto-unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TAutoFacade() {
        super(TAuto.class);
    }
    
    @Override
    public boolean isBodyAvailable(int id) {  
        BigInteger count = (BigInteger) em.createNativeQuery("select count(t.id) from T_AUTO t where ID_BODY=?").setParameter(1, id).getSingleResult();
        return (count.intValue() == 0);        
    }    

    @Override
    public boolean isEngineAvailable(int id) {                
        BigInteger count = (BigInteger) em.createNativeQuery("select count(t.id) from T_AUTO t where ID_ENGINE=?").setParameter(1, id).getSingleResult();
        return (count.intValue() == 0);        
    }    

    @Override
    public boolean isTransmissionAvailable(int id) {                
        BigInteger count = (BigInteger) em.createNativeQuery("select count(t.id) from T_AUTO t where ID_TRANSMISSION=?").setParameter(1, id).getSingleResult();
        return (count.intValue() == 0);        
    }   

    public void checkConstraints(
            int idBody,
            int idEngine,
            int idTransmission) throws Exception {
        String message = "";
        if (!isBodyAvailable(idBody)) {
            message += "Кузов недоступен! [id=" + idBody + "]";
        }
        if (!isEngineAvailable(idEngine)) {
            message += "Двигатель недоступен! [id=" + idEngine + "]";
        }
        if (!isTransmissionAvailable(idTransmission)) {
            message += "Трансмиссия недоступен! [id=" + idTransmission + "]";
        }        
        if (!message.isEmpty()) {
            throw new Exception(message);
        }
    }    
    
}
