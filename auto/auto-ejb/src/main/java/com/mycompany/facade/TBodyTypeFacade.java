package com.mycompany.facade;

import com.mycompany.faces.IBodyType;
import com.mycompany.entity.TBodyType;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author defender
 */
@Stateless
public class TBodyTypeFacade extends AFacade<TBodyType> implements IBodyType {

    @PersistenceContext(unitName = "auto-unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TBodyTypeFacade() {
        super(TBodyType.class);
    }
    
}
