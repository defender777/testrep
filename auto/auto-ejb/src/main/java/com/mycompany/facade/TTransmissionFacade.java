package com.mycompany.facade;

import com.mycompany.faces.ITransmission;
import com.mycompany.entity.TTransmission;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author defender
 */
@Stateless
public class TTransmissionFacade extends AFacade<TTransmission> implements ITransmission {

    @PersistenceContext(unitName = "auto-unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TTransmissionFacade() {
        super(TTransmission.class);
    }
    
}
