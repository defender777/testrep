package com.mycompany.facade;

import com.mycompany.faces.IEngine;
import com.mycompany.entity.TEngine;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author defender
 */
@Stateless
public class TEngineFacade extends AFacade<TEngine> implements IEngine {

    @PersistenceContext(unitName = "auto-unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TEngineFacade() {
        super(TEngine.class);
    }
    
}
