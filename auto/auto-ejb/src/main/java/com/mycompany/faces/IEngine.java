package com.mycompany.faces;

import com.mycompany.entity.TEngine;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author defender
 */
@Remote
public interface IEngine {

    void create(TEngine tEngine);

    void edit(TEngine tEngine);

    void remove(TEngine tEngine);

    TEngine find(Object id);

    List<TEngine> findAll();

    List<TEngine> findRange(int[] range);

    int count();
    
}
