package com.mycompany.faces;

import com.mycompany.entity.TBody;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author defender
 */
@Remote
public interface IBody {

    void create(TBody tBody);

    void edit(TBody tBody);

    void remove(TBody tBody);

    TBody find(Object id);

    List<TBody> findAll();

    boolean isAvailable(int id);
    
    List<TBody> findRange(int[] range);

    int count();
    
}
