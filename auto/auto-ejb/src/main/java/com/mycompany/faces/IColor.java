package com.mycompany.faces;

import com.mycompany.entity.TColor;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author defender
 */
@Remote
public interface IColor {

    void create(TColor tColor);

    void edit(TColor tColor);

    void remove(TColor tColor);

    TColor find(Object id);

    List<TColor> findAll();

    List<TColor> findRange(int[] range);

    int count();
    
}
