package com.mycompany.faces;

import com.mycompany.entity.TEngineType;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author defender
 */
@Remote
public interface IEngineType {

    void create(TEngineType tEngineType);

    void edit(TEngineType tEngineType);

    void remove(TEngineType tEngineType);

    TEngineType find(Object id);

    List<TEngineType> findAll();

    List<TEngineType> findRange(int[] range);

    int count();
    
}
