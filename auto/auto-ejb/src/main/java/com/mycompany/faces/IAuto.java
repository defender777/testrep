package com.mycompany.faces;

import com.mycompany.entity.TAuto;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author defender
 */
@Remote
public interface IAuto {

    void create(TAuto tAuto);

    void edit(TAuto tAuto);

    void remove(TAuto tAuto);

    TAuto find(Object id);

    List<TAuto> findAll();

    List<TAuto> findRange(int[] range);

    int count();
    
    boolean isBodyAvailable(int id);

    boolean isEngineAvailable(int id);        

    public boolean isTransmissionAvailable(int id);
    
    public void checkConstraints(
            int idBody,
            int idEngine,
            int idTransmission) throws Exception;
}
