package com.mycompany.faces;

import com.mycompany.entity.TBodyType;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author defender
 */
@Remote
public interface IBodyType {

    void create(TBodyType tBodyType);

    void edit(TBodyType tBodyType);

    void remove(TBodyType tBodyType);

    TBodyType find(Object id);

    List<TBodyType> findAll();

    List<TBodyType> findRange(int[] range);

    int count();
    
}
