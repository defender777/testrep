package com.mycompany.faces;

import com.mycompany.entity.TTransmission;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author defender
 */
@Remote
public interface ITransmission {

    void create(TTransmission tTransmission);

    void edit(TTransmission tTransmission);

    void remove(TTransmission tTransmission);

    TTransmission find(Object id);

    List<TTransmission> findAll();

    List<TTransmission> findRange(int[] range);

    int count();
    
}
