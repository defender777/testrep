package com.mycompany.faces;

import com.mycompany.entity.TTransmissionType;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author defender
 */
@Remote
public interface ITransmissionType {

    void create(TTransmissionType tTransmissionType);

    void edit(TTransmissionType tTransmissionType);

    void remove(TTransmissionType tTransmissionType);

    TTransmissionType find(Object id);

    List<TTransmissionType> findAll();

    List<TTransmissionType> findRange(int[] range);

    int count();
    
}
