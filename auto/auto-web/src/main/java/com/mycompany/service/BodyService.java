package com.mycompany.service;

import com.mycompany.entity.TBody;
import com.mycompany.faces.IBody;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author defender
 */
@Path("/color")
@Consumes({"application/json"})
@Produces({"application/json"})
public class BodyService {

    @EJB
    private IBody colorFacade;

    @PUT
    @Path("/create")
    public TBody create(TBody color) {
        color.setId(null);
        colorFacade.create(color);
        return color;
    }

    @POST
    @Path("/edit")
    public TBody edit(TBody color) {
            colorFacade.edit(color);
            return color;
    }

    @DELETE
    @Path("/remove")
    public TBody remove(TBody color) {
            colorFacade.remove(color);
            return color;
    }

    @GET
    @Path("/findAll")
    public List<TBody> findAll() {
        return colorFacade.findAll();
    }

    @GET
    @Path("/find/{id}")
    public TBody find(
            @PathParam("id") Integer id) {
        return colorFacade.find(id);
    }
}
