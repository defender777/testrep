package com.mycompany.service;

import com.mycompany.entity.TTransmission;
import com.mycompany.faces.ITransmission;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author defender
 */
@Path("/transmission")
@Consumes({"application/json"})
@Produces({"application/json"})
public class TransmissionService {

    @EJB
    private ITransmission transmissionFacade;

    @PUT
    @Path("/create")
    public TTransmission create(TTransmission transmission) {
        transmission.setId(null);
        transmissionFacade.create(transmission);
        return transmission;
    }

    @POST
    @Path("/edit")
    public TTransmission edit(TTransmission transmission) {
            transmissionFacade.edit(transmission);
            return transmission;
    }

    @DELETE
    @Path("/remove")
    public TTransmission remove(TTransmission transmission) {
            transmissionFacade.remove(transmission);
            return transmission;
    }

    @GET
    @Path("/findAll")
    public List<TTransmission> findAll() {
        return transmissionFacade.findAll();
    }

    @GET
    @Path("/find/{id}")
    public TTransmission find(
            @PathParam("id") Integer id) {
        return transmissionFacade.find(id);
    }
}
