package com.mycompany.service;

import com.mycompany.entity.TEngineType;
import com.mycompany.faces.IEngineType;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author defender
 */
@Path("/engineType")
@Consumes({"application/json"})
@Produces({"application/json"})
public class EngineTypeService {

    @EJB
    private IEngineType engineTypeFacade;

    @PUT
    @Path("/create")
    public TEngineType create(TEngineType engineType) {
        engineType.setId(null);
        engineTypeFacade.create(engineType);
        return engineType;
    }

    @POST
    @Path("/edit")
    public TEngineType edit(TEngineType engineType) {
            engineTypeFacade.edit(engineType);
            return engineType;
    }

    @DELETE
    @Path("/remove")
    public TEngineType remove(TEngineType engineType) {
            engineTypeFacade.remove(engineType);
            return engineType;
    }

    @GET
    @Path("/findAll")
    public List<TEngineType> findAll() {
        return engineTypeFacade.findAll();
    }

    @GET
    @Path("/find/{id}")
    public TEngineType find(
            @PathParam("id") Integer id) {
        return engineTypeFacade.find(id);
    }
}
