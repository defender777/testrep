package com.mycompany.service;

import com.mycompany.entity.TAuto;
import com.mycompany.faces.IAuto;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author defender
 */
@Path("/auto")
@Consumes({"application/json"})
@Produces({"application/json"})
public class AutoService {

    @EJB
    private IAuto autoFacade;

    @PUT
    @Path("/create")
    public TAuto create(TAuto auto) throws Exception {
        autoFacade.checkConstraints(
                auto.getIdBody(),
                auto.getIdEngine(),
                auto.getIdTransmission());
        auto.setId(null);
        autoFacade.create(auto);
        return auto;
    }

    @POST
    @Path("/edit")
    public TAuto edit(TAuto auto) throws Exception {
        autoFacade.checkConstraints(
                auto.getIdBody(),
                auto.getIdEngine(),
                auto.getIdTransmission());
        autoFacade.edit(auto);
        return auto;
    }

    @DELETE
    @Path("/remove")
    public TAuto remove(TAuto auto) {
        autoFacade.remove(auto);
        return auto;
    }

    @GET
    @Path("/findAll")
    public List<TAuto> findAll() {
        return autoFacade.findAll();
    }

    @GET
    @Path("/find/{id}")
    public TAuto find(
            @PathParam("id") Integer id) {
        return autoFacade.find(id);
    }
}
