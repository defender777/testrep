package com.mycompany.service;

import com.mycompany.entity.TColor;
import com.mycompany.faces.IColor;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author defender
 */
@Path("/color")
@Consumes({"application/json"})
@Produces({"application/json"})
public class ColorService {

    @EJB
    private IColor colorFacade;

    @PUT
    @Path("/create")
    public TColor create(TColor color) {
        color.setId(null);
        colorFacade.create(color);
        return color;
    }

    @POST
    @Path("/edit")
    public TColor edit(TColor color) {
            colorFacade.edit(color);
            return color;
    }

    @DELETE
    @Path("/remove")
    public TColor remove(TColor color) {
            colorFacade.remove(color);
            return color;
    }

    @GET
    @Path("/findAll")
    public List<TColor> findAll() {
        return colorFacade.findAll();
    }

    @GET
    @Path("/find/{id}")
    public TColor find(
            @PathParam("id") Integer id) {
        return colorFacade.find(id);
    }
}
