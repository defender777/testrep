package com.mycompany.service;

import com.mycompany.entity.TTransmissionType;
import com.mycompany.faces.ITransmissionType;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author defender
 */
@Path("/transmissionType")
@Consumes({"application/json"})
@Produces({"application/json"})
public class TransmissionTypeService {

    @EJB
    private ITransmissionType transmissionTypeFacade;

    @PUT
    @Path("/create")
    public TTransmissionType create(TTransmissionType transmissionType) {
        transmissionType.setId(null);
        transmissionTypeFacade.create(transmissionType);
        return transmissionType;
    }

    @POST
    @Path("/edit")
    public TTransmissionType edit(TTransmissionType transmissionType) {
            transmissionTypeFacade.edit(transmissionType);
            return transmissionType;
    }

    @DELETE
    @Path("/remove")
    public TTransmissionType remove(TTransmissionType transmissionType) {
            transmissionTypeFacade.remove(transmissionType);
            return transmissionType;
    }

    @GET
    @Path("/findAll")
    public List<TTransmissionType> findAll() {
        return transmissionTypeFacade.findAll();
    }

    @GET
    @Path("/find/{id}")
    public TTransmissionType find(
            @PathParam("id") Integer id) {
        return transmissionTypeFacade.find(id);
    }
}

