package com.mycompany.service;

import com.mycompany.entity.TEngine;
import com.mycompany.faces.IEngine;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author defender
 */
@Path("/engine")
@Consumes({"application/json"})
@Produces({"application/json"})
public class EngineService {

    @EJB
    private IEngine engineFacade;

    @PUT
    @Path("/create")
    public TEngine create(TEngine engine) {
        engine.setId(null);
        engineFacade.create(engine);
        return engine;
    }

    @POST
    @Path("/edit")
    public TEngine edit(TEngine engine) {
            engineFacade.edit(engine);
            return engine;
    }

    @DELETE
    @Path("/remove")
    public TEngine remove(TEngine engine) {
            engineFacade.remove(engine);
            return engine;
    }

    @GET
    @Path("/findAll")
    public List<TEngine> findAll() {
        return engineFacade.findAll();
    }

    @GET
    @Path("/find/{id}")
    public TEngine find(
            @PathParam("id") Integer id) {
        return engineFacade.find(id);
    }
}

