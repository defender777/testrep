package com.mycompany.service;

import com.mycompany.entity.TBodyType;
import com.mycompany.faces.IBodyType;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author defender
 */
@Path("/bodyType")
@Consumes({"application/json"})
@Produces({"application/json"})
public class BodyTypeService {

    @EJB
    private IBodyType bodyTypeFacade;

    @PUT
    @Path("/create")
    public TBodyType create(TBodyType bodyType) {
        bodyType.setId(null);
        bodyTypeFacade.create(bodyType);
        return bodyType;
    }

    @POST
    @Path("/edit")
    public TBodyType edit(TBodyType bodyType) {
            bodyTypeFacade.edit(bodyType);
            return bodyType;
    }

    @DELETE
    @Path("/remove")
    public TBodyType remove(TBodyType bodyType) {
            bodyTypeFacade.remove(bodyType);
            return bodyType;
    }

    @GET
    @Path("/findAll")
    public List<TBodyType> findAll() {
        return bodyTypeFacade.findAll();
    }

    @GET
    @Path("/find/{id}")
    public TBodyType find(
            @PathParam("id") Integer id) {
        return bodyTypeFacade.find(id);
    }
}

