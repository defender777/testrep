package com.mycompany.service.config;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author defender
 */
@javax.ws.rs.ApplicationPath("services")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.mycompany.service.AutoService.class);
        resources.add(com.mycompany.service.BodyService.class);
        resources.add(com.mycompany.service.BodyTypeService.class);
        resources.add(com.mycompany.service.ColorService.class);
        resources.add(com.mycompany.service.EngineService.class);
        resources.add(com.mycompany.service.EngineTypeService.class);
        resources.add(com.mycompany.service.TransmissionService.class);
        resources.add(com.mycompany.service.TransmissionTypeService.class);
    }
    
}
