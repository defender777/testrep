package com.mycompany.cdi;

import com.mycompany.entity.TBodyType;
import com.mycompany.faces.IBodyType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;


/**
 *
 * @author defender
 */
@Named(value = "bodyTypeBean")
@SessionScoped
public class BodyTypeBean  implements Serializable {
    private static final String TABLE = "bodyTypeTable";
    private static final String CARD = "bodyTypeCard";

    private List<TBodyType> items;
    private TBodyType selectedItem;
    
    @EJB
    private IBodyType bodyTypeFacade;    

    public BodyTypeBean() {
    }
    
    @PostConstruct
    void init() {
        refresh();
    }
    
    public String refresh() {
        setItems(bodyTypeFacade.findAll());
        return null;
    }

    public String addItem() {
        selectedItem = new TBodyType();
        return CARD;
    }

    public String editItem(TBodyType bodyType) {
        selectedItem = bodyType;
        return CARD;
    }

    public String deleteItem(TBodyType bodyType) {
        items.remove(bodyType);
        bodyTypeFacade.remove(bodyType);
        return null;
    }
    
    public String saveItem() {
        if (items.indexOf(selectedItem) != -1) {
            bodyTypeFacade.edit(selectedItem);
        } else {
            items.add(selectedItem);
            bodyTypeFacade.create(selectedItem);
        }
        refresh();
        return TABLE;
    }    
    
    public String cancelItem() {
        return TABLE;
    }    
    
    public List<TBodyType> getItems() {
        return items;
    }
        
    private void setItems(List<TBodyType> items) {
        this.items = items;        
    }

    public TBodyType getSelectedItem() {
        return selectedItem;
    }
    public List<SelectItem> getBodyTypes() {
        List<SelectItem> res = new ArrayList<>();
        refresh();
        for (TBodyType bodyType : getItems()) {
            res.add(new SelectItem(bodyType.getId(), bodyType.getName()));
        }
        return res;
    }       
}

