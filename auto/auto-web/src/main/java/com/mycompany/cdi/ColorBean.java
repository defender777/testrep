package com.mycompany.cdi;

import com.mycompany.entity.TColor;
import com.mycompany.faces.IColor;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;


/**
 *
 * @author defender
 */
@Named(value = "colorBean")
@SessionScoped
public class ColorBean  implements Serializable {
    private static final String TABLE = "colorTable";
    private static final String CARD = "colorCard";

    private List<TColor> items;
    private TColor selectedItem;
    
    @EJB
    private IColor colorFacade;    

    public ColorBean() {
    }
    
    @PostConstruct
    void init() {
        refresh();
    }
    
    public String refresh() {
        setItems(colorFacade.findAll());
        return null;
    }

    public String addItem() {
        selectedItem = new TColor();
        return CARD;
    }

    public String editItem(TColor color) {
        selectedItem = color;
        return CARD;
    }

    public String deleteItem(TColor color) {
        items.remove(color);
        colorFacade.remove(color);
        return null;
    }
    
    public String saveItem() {
        if (items.indexOf(selectedItem) != -1) {
            colorFacade.edit(selectedItem);
        } else {
            items.add(selectedItem);
            colorFacade.create(selectedItem);
        }
        refresh();
        return TABLE;
    }    
    
    public String cancelItem() {
        return TABLE;
    }    
    
    public List<TColor> getItems() {
        return items;
    }
        
    private void setItems(List<TColor> items) {
        this.items = items;        
    }

    public TColor getSelectedItem() {
        return selectedItem;
    }  
    
    public List<SelectItem> getColors() {
        List<SelectItem> res = new ArrayList<>();
        refresh();
        for (TColor color : getItems()) {
            res.add(new SelectItem(color.getId(), color.getName()));
        }
        return res;
    }     
}
