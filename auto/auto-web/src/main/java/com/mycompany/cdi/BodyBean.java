package com.mycompany.cdi;

import com.mycompany.entity.TBody;
import com.mycompany.faces.IBody;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;


/**
 *
 * @author defender
 */
@Named(value = "bodyBean")
@SessionScoped
public class BodyBean  implements Serializable {
    private static final String TABLE = "bodyTable";
    private static final String CARD = "bodyCard";

    private List<TBody> items;
    private TBody selectedItem;
    private String selectedBodyType;    
    private String selectedColor;    
    
    @EJB
    private IBody bodyFacade;    

    public BodyBean() {
    }
    
    @PostConstruct
    void init() {
        refresh();
    }
    
    public String refresh() {
        setItems(bodyFacade.findAll());        
        return null;
    }       

    public String addItem() {
        selectedItem = new TBody();
        return CARD;
    }

    public String editItem(TBody body) {
        selectedItem = body;
        setSelectedBodyType(Integer.toString(body.getIdBodyType()));
        setSelectedColor(Integer.toString(body.getIdColor()));
        return CARD;
    }

    public String deleteItem(TBody body) {
        items.remove(body);
        bodyFacade.remove(body);
        return null;
    }
    
    public String saveItem() {
        selectedItem.setIdBodyType(Integer.parseInt(selectedBodyType));
        selectedItem.setIdColor(Integer.parseInt(selectedColor));
        if (items.indexOf(selectedItem) != -1) {
            bodyFacade.edit(selectedItem);
        } else {            
            items.add(selectedItem);
            bodyFacade.create(selectedItem);
        }
        refresh();
        setSelectedBodyType(null);
        setSelectedColor(null);
        return TABLE;
    }    
    
    public String cancelItem() {
        setSelectedBodyType(null);
        setSelectedColor(null);
        return TABLE;
    }    
    
    public List<TBody> getItems() {
        return items;
    }
        
    private void setItems(List<TBody> items) {
        this.items = items;        
    }

    public TBody getSelectedItem() {
        return selectedItem;
    }  

    public String getSelectedBodyType() {
        return selectedBodyType;
    }

    public void setSelectedBodyType(String selectedBodyType) {
        this.selectedBodyType = selectedBodyType;
    }        

    public String getSelectedColor() {
        return selectedColor;
    }

    public void setSelectedColor(String selectedColor) {
        this.selectedColor = selectedColor;
    }
    
    public List<SelectItem> getBodyes() {
        List<SelectItem> res = new ArrayList<>();
        for (TBody body : getItems()) {
            res.add(new SelectItem(body.getId(), body.getVin()));
        }
        return res;
    }     
}
