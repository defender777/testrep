package com.mycompany.cdi;

import com.mycompany.entity.TTransmission;
import com.mycompany.faces.ITransmission;
import com.mycompany.faces.ITransmissionType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;


/**
 *
 * @author defender
 */
@Named(value = "transmissionBean")
@SessionScoped
public class TransmissionBean  implements Serializable {
    private static final String TABLE = "transmissionTable";
    private static final String CARD = "transmissionCard";

    private List<TTransmission> items;
    private TTransmission selectedItem;
    private String selectedTransmissionType;    
    
    @EJB
    private ITransmission transmissionFacade;    

    public TransmissionBean() {
    }
    
    @PostConstruct
    void init() {
        refresh();
    }
    
    public String refresh() {
        setItems(transmissionFacade.findAll());
        return null;
    }

    public String addItem() {
        selectedItem = new TTransmission();
        return CARD;
    }

    public String editItem(TTransmission transmission) {
        selectedItem = transmission;
        setSelectedTransmissionType(Integer.toString(transmission.getIdTransmissionType()));
        return CARD;
    }

    public String deleteItem(TTransmission transmission) {
        items.remove(transmission);
        transmissionFacade.remove(transmission);
        return null;
    }
    
    public String saveItem() {
        selectedItem.setIdTransmissionType(Integer.parseInt(selectedTransmissionType));
        if (items.indexOf(selectedItem) != -1) {
            transmissionFacade.edit(selectedItem);
        } else {            
            items.add(selectedItem);
            transmissionFacade.create(selectedItem);
        }
        refresh();
        setSelectedTransmissionType(null);
        return TABLE;
    }    
    
    public String cancelItem() {
        setSelectedTransmissionType(null);
        return TABLE;
    }    
    
    public List<TTransmission> getItems() {
        return items;
    }
        
    private void setItems(List<TTransmission> items) {
        this.items = items;        
    }

    public TTransmission getSelectedItem() {
        return selectedItem;
    }  

    public String getSelectedTransmissionType() {
        return selectedTransmissionType;
    }

    public void setSelectedTransmissionType(String selectedTransmissionType) {
        this.selectedTransmissionType = selectedTransmissionType;
    }    

    public List<SelectItem> getTransmissions() {
        List<SelectItem> res = new ArrayList<>();
        refresh();
        for (TTransmission transmission : getItems()) {
            res.add(new SelectItem(transmission.getId(), transmission.getSn()));
        }
        return res;
    }     
    
}
