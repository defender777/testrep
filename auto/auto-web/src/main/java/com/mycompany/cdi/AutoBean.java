package com.mycompany.cdi;

import com.mycompany.entity.TAuto;
import com.mycompany.faces.IAuto;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;


/**
 *
 * @author defender
 */
@Named(value = "autoBean")
@SessionScoped
public class AutoBean  implements Serializable {
    private static final String TABLE = "autoTable";
    private static final String CARD = "autoCard";

    private List<TAuto> items;
    private TAuto selectedItem;
    private String selectedBody;    
    private String selectedEngine;    
    private String selectedTransmission;    
    
    @EJB
    private IAuto autoFacade;    

    public AutoBean() {
    }
    
    @PostConstruct
    void init() {
        refresh();
    }
    
    public String refresh() {
        setItems(autoFacade.findAll());
        return null;
    }

    public String addItem() {
        selectedItem = new TAuto();
        return CARD;
    }

    public String editItem(TAuto auto) {
        selectedItem = auto;
        setSelectedBody(Integer.toString(auto.getIdBody()));
        setSelectedEngine(Integer.toString(auto.getIdEngine()));
        setSelectedTransmission(Integer.toString(auto.getIdTransmission()));
        return CARD;
    }

    public String deleteItem(TAuto auto) {
        items.remove(auto);
        autoFacade.remove(auto);
        return null;
    }
    
    public String saveItem() throws Exception {
        selectedItem.setIdBody(Integer.parseInt(selectedBody));
        selectedItem.setIdEngine(Integer.parseInt(selectedEngine));
        selectedItem.setIdTransmission(Integer.parseInt(selectedTransmission));
        if (items.indexOf(selectedItem) != -1) {
            autoFacade.edit(selectedItem);
        } else {            
            items.add(selectedItem);
            autoFacade.create(selectedItem);
        }
        refresh();
        setSelectedBody(null);
        setSelectedEngine(null);
        setSelectedTransmission(null);
        return TABLE;
    }    
   
    public String cancelItem() {
        setSelectedBody(null);
        setSelectedEngine(null);
        setSelectedTransmission(null);
        return TABLE;
    }    
    
    public List<TAuto> getItems() {
        return items;
    }
        
    private void setItems(List<TAuto> items) {
        this.items = items;        
    }

    public TAuto getSelectedItem() {
        return selectedItem;
    }

    public String getSelectedBody() {
        return selectedBody;
    }

    public void setSelectedBody(String selectedBody) {
        this.selectedBody = selectedBody;
    }

    public String getSelectedEngine() {
        return selectedEngine;
    }

    public void setSelectedEngine(String selectedEngine) {
        this.selectedEngine = selectedEngine;
    }

    public String getSelectedTransmission() {
        return selectedTransmission;
    }

    public void setSelectedTransmission(String selectedTransmission) {
        this.selectedTransmission = selectedTransmission;
    }            
}

