package com.mycompany.cdi;

import com.mycompany.entity.TEngine;
import com.mycompany.faces.IEngine;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;


/**
 *
 * @author defender
 */
@Named(value = "engineBean")
@SessionScoped
public class EngineBean  implements Serializable {
    private static final String TABLE = "engineTable";
    private static final String CARD = "engineCard";

    private List<TEngine> items;
    private TEngine selectedItem;
    private String selectedEngineType;    
    
    @EJB
    private IEngine engineFacade;    

    public EngineBean() {
    }
    
    @PostConstruct
    void init() {
        refresh();
    }
    
    public String refresh() {
        setItems(engineFacade.findAll());
        return null;
    }

    public String addItem() {
        selectedItem = new TEngine();
        return CARD;
    }

    public String editItem(TEngine engine) {
        selectedItem = engine;
        setSelectedEngineType(Integer.toString(engine.getIdEngineType()));
        return CARD;
    }

    public String deleteItem(TEngine engine) {
        items.remove(engine);
        engineFacade.remove(engine);
        return null;
    }
    
    public String saveItem() {
        selectedItem.setIdEngineType(Integer.parseInt(selectedEngineType));
        if (items.indexOf(selectedItem) != -1) {
            engineFacade.edit(selectedItem);
        } else {            
            items.add(selectedItem);
            engineFacade.create(selectedItem);
        }
        refresh();
        setSelectedEngineType(null);
        return TABLE;
    }    
    
    public String cancelItem() {
        setSelectedEngineType(null);
        return TABLE;
    }    
    
    public List<TEngine> getItems() {
        return items;
    }
        
    private void setItems(List<TEngine> items) {
        this.items = items;        
    }

    public TEngine getSelectedItem() {
        return selectedItem;
    }  

    public String getSelectedEngineType() {
        return selectedEngineType;
    }

    public void setSelectedEngineType(String selectedEngineType) {
        this.selectedEngineType = selectedEngineType;
    } 
    
    public List<SelectItem> getEngines() {
        List<SelectItem> res = new ArrayList<>();
        refresh();
        for (TEngine engine : getItems()) {
            res.add(new SelectItem(engine.getId(), engine.getSn()));
        }
        return res;
    }         
}
