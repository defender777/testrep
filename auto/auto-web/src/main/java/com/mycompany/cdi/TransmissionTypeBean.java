package com.mycompany.cdi;

import com.mycompany.entity.TTransmissionType;
import com.mycompany.faces.ITransmissionType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;


/**
 *
 * @author defender
 */
@Named(value = "transmissionTypeBean")
@SessionScoped
public class TransmissionTypeBean  implements Serializable {
    private static final String TABLE = "transmissionTypeTable";
    private static final String CARD = "transmissionTypeCard";

    private List<TTransmissionType> items;
    private TTransmissionType selectedItem;
    
    @EJB
    private ITransmissionType transmissionTypeFacade;    

    public TransmissionTypeBean() {
    }
    
    @PostConstruct
    void init() {
        refresh();
    }
    
    public String refresh() {
        setItems(transmissionTypeFacade.findAll());
        return null;
    }

    public String addItem() {
        selectedItem = new TTransmissionType();
        return CARD;
    }

    public String editItem(TTransmissionType transmissionType) {
        selectedItem = transmissionType;
        return CARD;
    }

    public String deleteItem(TTransmissionType transmissionType) {
        items.remove(transmissionType);
        transmissionTypeFacade.remove(transmissionType);
        return null;
    }
    
    public String saveItem() {
        if (items.indexOf(selectedItem) != -1) {
            transmissionTypeFacade.edit(selectedItem);
        } else {
            items.add(selectedItem);
            transmissionTypeFacade.create(selectedItem);
        }
        refresh();
        return TABLE;
    }    
    
    public String cancelItem() {
        return TABLE;
    }    
    
    public List<TTransmissionType> getItems() {
        return items;
    }
        
    private void setItems(List<TTransmissionType> items) {
        this.items = items;        
    }

    public TTransmissionType getSelectedItem() {
        return selectedItem;
    } 
    
    public List<SelectItem> getTransmissionTypes() {
        List<SelectItem> res = new ArrayList<>();
        refresh();
        for (TTransmissionType transmissionType : getItems()) {
            res.add(new SelectItem(transmissionType.getId(), transmissionType.getName()));
        }
        return res;
    }          
}
