package com.mycompany.cdi;

import com.mycompany.entity.TEngineType;
import com.mycompany.faces.IEngineType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;


/**
 *
 * @author defender
 */
@Named(value = "engineTypeBean")
@SessionScoped
public class EngineTypeBean  implements Serializable {
    private static final String TABLE = "engineTypeTable";
    private static final String CARD = "engineTypeCard";

    private List<TEngineType> items;
    private TEngineType selectedItem;
    
    @EJB
    private IEngineType engineTypeFacade;    

    public EngineTypeBean() {
    }
    
    @PostConstruct
    void init() {
        refresh();
    }
    
    public String refresh() {
        setItems(engineTypeFacade.findAll());
        return null;
    }

    public String addItem() {
        selectedItem = new TEngineType();
        return CARD;
    }

    public String editItem(TEngineType engineType) {
        selectedItem = engineType;
        return CARD;
    }

    public String deleteItem(TEngineType engineType) {
        items.remove(engineType);
        engineTypeFacade.remove(engineType);
        return null;
    }
    
    public String saveItem() {
        if (items.indexOf(selectedItem) != -1) {
            engineTypeFacade.edit(selectedItem);
        } else {
            items.add(selectedItem);
            engineTypeFacade.create(selectedItem);
        }
        refresh();
        return TABLE;
    }    
    
    public String cancelItem() {
        return TABLE;
    }    
    
    public List<TEngineType> getItems() {
        return items;
    }
        
    private void setItems(List<TEngineType> items) {
        this.items = items;        
    }

    public TEngineType getSelectedItem() {
        return selectedItem;
    }
    public List<SelectItem> getEngineTypes() {
        List<SelectItem> res = new ArrayList<>();
        refresh();
        for (TEngineType engineType : getItems()) {
            res.add(new SelectItem(engineType.getId(), engineType.getName()));
        }
        return res;
    }          
    
}

