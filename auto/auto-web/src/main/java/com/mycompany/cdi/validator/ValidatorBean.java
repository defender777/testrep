package com.mycompany.cdi.validator;

import com.mycompany.faces.IAuto;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

/**
 *
 * @author defender
 */
@Named(value = "validatorBean")
@SessionScoped
public class ValidatorBean implements Serializable {
    @EJB
    private IAuto autoFacade;    
    
    public void checkAvailableBody(
            FacesContext context, 
            UIComponent component, 
            Object value) throws ValidatorException {        
        if (!autoFacade.isBodyAvailable(Integer.parseInt((String) value))) {
            FacesMessage msg
                    = new FacesMessage(
                            "Кузов уже установлен!",
                            "Кузов не доступен для установки!");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }
    }
    public void checkAvailableEngine(
            FacesContext context, 
            UIComponent component, 
            Object value) throws ValidatorException {        
        if (!autoFacade.isEngineAvailable(Integer.parseInt((String) value))) {
            FacesMessage msg
                    = new FacesMessage(
                            "Двигатель уже установлен!",
                            "Двигатель не доступен для установки!");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }
    }
    public void checkAvailableTransmission(
            FacesContext context, 
            UIComponent component, 
            Object value) throws ValidatorException {        
        if (!autoFacade.isTransmissionAvailable(Integer.parseInt((String) value))) {
            FacesMessage msg
                    = new FacesMessage(
                            "Трансмиссия уже установлена!",
                            "Трансмиссия не доступна для установки!");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }
    }        
}
